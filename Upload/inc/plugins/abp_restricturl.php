<?php

/**
 * Restrict usage of URL for new users
 * Copyright 2019 CrazyCat <crazycat@c-p-f.org>
 */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}
define('CN_ABPRURL', str_replace('.php', '', basename(__FILE__)));

function abp_restricturl_info() {
    global $lang;
    $lang->load(CN_ABPRURL);
    return array(
        'name' => $lang->abp_restricturl_name,
        'description' => $lang->abp_restricturl_desc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
        'website' => 'https://gitlab.com/ab-plugins/abp-restrict-url',
        'author' => 'CrazyCat',
        'authorsite' => 'http://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '1.3',
        'compatibility' => '18*',
        'codename' => CN_ABPRURL
    );
}

function abp_restricturl_install() {
    global $db, $lang;
    $lang->load(CN_ABPRURL);
    $settinggroups = [
        'name' => CN_ABPRURL,
        'title' => $lang->abp_restricturl_setting_title,
        'description' => $lang->abp_restricturl_setting_desc,
        'disporder' => 0,
        'isdefault' => 0
    ];
    $db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();
    abp_restricturl_upgrade($gid);
    rebuild_settings();
}

function abp_restricturl_is_installed() {
    global $mybb;
    return (array_key_exists(CN_ABPRURL . '_nbpost', $mybb->settings));
}

function abp_restricturl_uninstall() {
    global $db;
    $db->delete_query('settings', "name LIKE '%" . CN_ABPRURL . "%'");
    $db->delete_query('settinggroups', "name = '" . CN_ABPRURL . "'");
    rebuild_settings();
}

function abp_restricturl_activate() {
    abp_restricturl_upgrade();
    rebuild_settings();
}

function abp_restricturl_deactivate() {
    rebuild_settings();
}

function abp_restricturl_upgrade($gid = 0) {
    global $lang, $db;
    $lang->load(CN_ABPRURL);
    $settings = [
        [
            'name' => CN_ABPRURL . '_gexcluded',
            'title' => $lang->abp_restricturl_gexcluded,
            'description' => $lang->abp_restricturl_gexcluded_desc,
            'optionscode' => 'groupselect',
            'value' => '3,4,6',
            'disporder' => 1
        ],
        [
            'name' => CN_ABPRURL . '_nbpost',
            'title' => $lang->abp_restricturl_nbpost,
            'description' => $lang->abp_restricturl_nbpost_desc,
            'optionscode' => 'numeric' . PHP_EOL . 'min=1',
            'value' => '5',
            'disporder' => 2
        ],
        [
            'name' => CN_ABPRURL . '_internal',
            'title' => $lang->abp_restricturl_internal,
            'description' => $lang->abp_restricturl_internal_desc,
            'optionscode' => 'yesno',
            'value' => 1,
            'disporder' => 3
        ],
        [
            'name' => CN_ABPRURL . '_img',
            'title' => $lang->abp_restricturl_img,
            'description' => $lang->abp_restricturl_img_desc,
            'optionscode' => 'yesno',
            'value' => 1,
            'disporder' => 3
        ],
        [
            'name' => CN_ABPRURL . '_quote',
            'title' => $lang->abp_restricturl_quote,
            'description' => $lang->abp_restricturl_quote_desc,
            'optionscode' => 'yesno',
            'value' => 1,
            'disporder' => 4
        ],
        [
            'name' => CN_ABPRURL . '_sign',
            'title' => $lang->abp_restricturl_sign,
            'description' => $lang->abp_restricturl_sign_desc,
            'optionscode' => 'yesno',
            'value' => 0,
            'disporder' => 6
        ],
        [
            'name' => CN_ABPRURL . '_homepage',
            'title' => $lang->abp_restricturl_homepage,
            'description' => $lang->abp_restricturl_homepage_desc,
            'optionscode' => 'yesno',
            'value' => 0,
            'disporder' => 7
        ],
        [
            'name' => CN_ABPRURL . '_pm',
            'title' => $lang->abp_restricturl_pm,
            'description' => $lang->abp_restricturl_pm_desc,
            'optionscode' => 'yesno',
            'value' => 0,
            'disporder' => 7
        ],
        [
            'name' => CN_ABPRURL . '_email',
            'title' => $lang->abp_restricturl_email,
            'description' => $lang->abp_restricturl_email_desc,
            'optionscode' => 'yesno',
            'value' => 0,
            'disporder' => 7
        ],
        [
            'name' => CN_ABPRURL . '_authurl',
            'title' => $lang->abp_restricturl_authurl,
            'description' => $lang->abp_restricturl_authurl_desc,
            'optionscode' => 'textarea',
            'value' => '',
            'disporder' => 8
        ]
    ];
    $osettings = [];
    if ((int) $gid == 0) {
        $query = $db->simple_select('settings', 'name, gid', "name LIKE '" . CN_ABPRURL . "%'");
        while ($setted = $db->fetch_array($query)) {
            $osettings[] = $setted['name'];
            $gid = $setted['gid'];
        }
    }
    foreach ($settings as $i => $setting) {
        if (in_array($setting['name'], $osettings)) {
            continue;
        }
        $insert = [
            'name' => $db->escape_string($setting['name']),
            'title' => $db->escape_string($setting['title']),
            'description' => $db->escape_string($setting['description']),
            'optionscode' => $db->escape_string($setting['optionscode']),
            'value' => $db->escape_string($setting['value']),
            'disporder' => $setting['disporder'],
            'gid' => $gid,
        ];
        $db->insert_query('settings', $insert);
    }
}

$plugins->add_hook('datahandler_post_validate_post', 'abp_urlrestrict_postcheck');
$plugins->add_hook('datahandler_post_validate_thread', 'abp_urlrestrict_postcheck');

function abp_urlrestrict_postcheck(&$datahandler) {
    global $mybb, $lang;
    if (!isset($datahandler->data['message'])) {
        return;
    }
	if (!isset($datahandler->data['uid']) && isset($datahandler->data['edit_uid'])) {
		$puser = get_user($datahandler->data['edit_uid']);
	} else {
		$puser = get_user($datahandler->data['uid']);
	}
    if (!abp_urlrestrict_needcheck($puser['uid'], $puser['postnum'])) {
        return;
    }
    $lang->load(CN_ABPRURL);
    $message = $datahandler->data['message'];
    if ($mybb->settings[CN_ABPRURL . '_img'] == 1) {
        // Pictures allowed
        $message = preg_replace('#\[img.+/img\]#isU', '', $message);
    }
    if ($mybb->settings[CN_ABPRURL . '_internal']) {
        $message = str_ireplace($mybb->settings['bburl'], '', $message);
    }
    if ($mybb->settings[CN_ABPRURL . '_quote'] == 1) {
        // Urls allowed in quotes : clean quotes
        $message = preg_replace('#\[quote.+/quote\]#isU', '', $message);
    }
    $alloweds = explode(PHP_EOL, $mybb->settings[CN_ABPRURL . '_authurl']);

    $validation = !abp_urlrestrict_validate($message, $alloweds);
    if ($validation === false) {
        $datahandler->set_error($lang->abp_urlrestrict_post);
    }
}

$plugins->add_hook('usercp_do_editsig_start', 'abp_urlrestrict_sign');

function abp_urlrestrict_sign() {
    global $mybb, $lang;
    if (!abp_urlrestrict_needcheck($mybb->user['uid'], $mybb->user['postnum'])) {
        return;
    }
    $lang->load(CN_ABPRURL);
    $signature = trim($mybb->input['signature']);
    if ($mybb->settings[CN_ABPRURL . '_img'] == 1) {
        // Pictures allowed
        $signature = preg_replace('#\[img.+/img\]#isU', '', $signature);
    }
    if ($mybb->settings[CN_ABPRURL . '_internal']) {
        $signature = str_ireplace($mybb->settings['bburl'], '', $signature);
    }
    $alloweds = explode(PHP_EOL, $mybb->settings[CN_ABPRURL . '_authurl']);

    $validation = !abp_urlrestrict_validate($signature, $alloweds);
    if ($validation === false) {
        redirect('usercp.php?action=editsig', $lang->abp_urlrestrict_sign, '', true);
    }
}

$plugins->add_hook('postbit', 'abp_urlrestrict_postbit');

function abp_urlrestrict_postbit(&$post) {
    global $mybb;
    if ($mybb->settings[CN_ABPRURL . '_homepage'] != 1 && abp_urlrestrict_needcheck($post['uid'], $post['postnum'])) {
        $post['button_www'] = '';
    }
}

$plugins->add_hook('datahandler_pm_validate', 'abp_urlrestrict_pm');

function abp_urlrestrict_pm(&$pm) {
    global $mybb, $lang;
    $lang->load(CN_ABPRURL);
    if ($mybb->settings[CN_ABPRURL . '_pm'] != 1 && abp_urlrestrict_needcheck($pm->data['fromid'], $mybb->user['postnum'])) {
        $alloweds = explode(PHP_EOL, $mybb->settings[CN_ABPRURL . '_authurl']);
        $content = $pm->data['subject'] . ' ' . $pm->data['message'];
        if (abp_urlrestrict_validate($content, $alloweds)) {
            $pm->set_error($lang->abp_urlrestrict_pm);
        }
    }
}

$plugins->add_hook('member_do_emailuser_start', 'abp_urlrestrict_email');

function abp_urlrestrict_email() {
    global $mybb, $lang;
    $lang->load(CN_ABPRURL);
    if ($mybb->settings[CN_ABPRURL . '_email'] != 1 && abp_urlrestrict_needcheck($mybb->user['uid'], $mybb->user['postnum'])) {
        $alloweds = explode(PHP_EOL, $mybb->settings[CN_ABPRURL . '_authurl']);
        $content = $mybb->input['subject'] . ' ' . $mybb->input['message'];
        if (abp_urlrestrict_validate($content, $alloweds)) {
            error($lang->abp_urlrestrict_email);
        }
    }
}

$plugins->add_hook('member_profile_start', 'abp_restricturl_profile');

function abp_restricturl_profile() {
    global $mybb;
    if ($mybb->input['action'] != 'profile') {
        return;
    }
    if ($mybb->usergroup['issupermod'] == 1 || $mybb->usergroup['canmodcp'] == 1) {
        return;
    }
    $uid = $mybb->get_input('uid', MyBB::INPUT_INT);
    if ($uid) {
        $memprofile = get_user($uid);
    } else {
        $memprofile = $mybb->user;
    }
    if (abp_urlrestrict_needcheck($memprofile['uid'], $memprofile['postnum'])) {
        $mybb->settings['hidewebsite'] = $mybb->user['usergroup'];
    }
}

function abp_urlrestrict_validate($content, $alloweds = []) {
    if (count($alloweds) > 0) {
        $content = str_ireplace($alloweds, '', $content);
    }
	$patterns = [
		"#<a\\s[^>]*>.*?</a>|(http|https|ftp|news|irc|ircs|irc6){1}(://)([^\/\"\s\<\[\.]+\.([^\/\"\s\<\[\.]+\.)*[\w]+(:[0-9]+)?(/([^\"\s<\[]|\[\])*)?([\w\/\)]))#ius",
		"#<a\\s[^>]*>.*?</a>|(www|ftp)(\.)(([^\/\"\s\<\[\.]+\.)*[\w]+(:[0-9]+)?(/([^\"\s<\[]|\[\])*)?([\w\/\)]))#ius",
		"#\[url[^\]*]#ius"
	];
    $hasurl = false;
	foreach($patterns as $pattern) {
		if (preg_match_all($pattern, $content, $matches)) {
			$hasurl = true;
		}
	}
    return $hasurl;
}

function abp_urlrestrict_needcheck($uid, $postnum = 0) {
    global $mybb;
    // First check : is user in excluded group ?
    if (is_member($mybb->settings[CN_ABPRURL . '_gexcluded'], $uid)) {
        return false;
    }
    // Second check : has user enough posts ?
    if ((int) $postnum >= (int) $mybb->settings[CN_ABPRURL . '_nbpost']) {
        return false;
    }
    return true;
}
