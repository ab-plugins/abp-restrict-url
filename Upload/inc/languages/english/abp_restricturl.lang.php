<?php
$l['abp_urlrestrict_post'] = 'You\'re not allowed to put url in the post';
$l['abp_urlrestrict_sign'] = 'You\'re not allowed to put url in your signature';
$l['abp_urlrestrict_pm'] = 'You\'re not allowed to put url in PM';
$l['abp_urlrestrict_email'] = 'You\'re not allowed to put url in email';