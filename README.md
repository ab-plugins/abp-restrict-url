# ABP Restrict Url

Forbid url in post, signature and website for new users

# Settings
* [x] Number of post until the user is no more a new one : from 1 to ... default 5
* [x] Groups excluded of the restriction : default admins, moderators
* [x] Allow pictures : default Y
* [x] Allow url in quotes : default Y
* [x] Allow url in signature : default N
* [x] Allow display of homepage : default N
* [x] Allow url in PM : default N
* [x] Allow url in email : default N
* [x] Allow internal urls : default Y
* [x] List of authorized urls :

# Notes
* The user website is allowed to be filled, but will be hide on display (posts and profile).
 
# TODO
* Allow admins and moderators to see the website in member profile