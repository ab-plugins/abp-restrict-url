<?php
// General
$l['abp_restricturl_name'] = 'ABP Restrict url';
$l['abp_restricturl_desc'] = 'Restrict url posting for new users';

// Settings
$l['abp_restricturl_setting_title'] = 'Settings for ABP Restrict url';
$l['abp_restricturl_setting_desc'] = 'Choose the criteria for the restriction';

$l['abp_restricturl_gexcluded'] = 'Excluded groups';
$l['abp_restricturl_gexcluded_desc'] = 'The selected groups won\'t be affected by the restriction.';
$l['abp_restricturl_nbpost'] = 'Number of posts';
$l['abp_restricturl_nbpost_desc'] = 'Number of posts required to allow user to post url.';
$l['abp_restricturl_internal'] = 'Internal url';
$l['abp_restricturl_internal_desc'] = 'Allow internal linking ?';
$l['abp_restricturl_img'] = 'Pictures';
$l['abp_restricturl_img_desc'] = 'Allow pictures ?';
$l['abp_restricturl_quote'] = 'Quotes';
$l['abp_restricturl_quote_desc'] = '<span class="alert" style="display:inline-block;">Allow urls in quotes ? No verification will be made in quote blocks</span>';
$l['abp_restricturl_sign'] = 'Signature';
$l['abp_restricturl_sign_desc'] = 'Allow url in signature ?';
$l['abp_restricturl_homepage'] = 'User homepage';
$l['abp_restricturl_homepage_desc'] = 'Allow the user homepage ?<br />This setting only hide the display of the homepage link in posts and profile, it doesn\'t forbid it in User CP.';
$l['abp_restricturl_pm'] = 'Private messaging';
$l['abp_restricturl_pm_desc'] = 'Allow user to put links in private messaging.';
$l['abp_restricturl_email'] = 'Email';
$l['abp_restricturl_email_desc'] = 'Allow user to put links in email.';
$l['abp_restricturl_authurl'] = 'Authorized urls';
$l['abp_restricturl_authurl_desc'] = 'List of the authorized domains, one per line with leading http(s)://. Example: https://mybb.com';